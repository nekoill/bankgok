package main

import NewAPIServer "gitlab.com/nekoill/bankgok/pkg/api"

func main() {
	server := NewAPIServer(":3000")
	server.Run()
}
