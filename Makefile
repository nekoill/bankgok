build:
	@go build -o ./bin/bankgok

run: build
	@./bin/bankgok

test:
	@go test -v ./..